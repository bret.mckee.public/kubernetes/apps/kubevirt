# KubeVirt

Kubevirt does not directly support Kustomize, so this repository servers as an intermeidary.

[These
instructions](https://objects.githubusercontent.com/github-production-release-asset-2e65be/76686583/7546abfb-84da-4d91-8631-5d4e710ada36?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIWNJYAX4CSVEH53A%2F20230208%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=20230208T031540Z&X-Amz-Expires=300&X-Amz-Signature=01a0d9f2d6a73b944a19e42d8c327a9dc3c98b1dab49ac2b4ed32d275f16cefa&X-Amz-SignedHeaders=host&actor_id=0&key_id=0&repo_id=76686583&response-content-disposition=attachment%3B%20filename%3Dkubevirt-operator.yaml&response-content-type=application%2Foctet-stream)
were used to create the files in resources using wget from version v0.58.0.

